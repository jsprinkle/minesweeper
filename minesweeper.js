function init(length, width, mines) {
	length = Math.floor(length)
	width = Math.floor(width)
	mines = Math.floor(mines)

	if (length > 26 || width > 40 || length < 5 || width < 15 || mines > (0.9 * (length * width)) || mines < 1) {
		return null
	}

	//0: safe, 1: mine, 2: revealed
	let field = []
	for (let i = 0; i < length; i++) {
		field.push([])
		for (let j = 0; j < width; j++) {
			field[i].push(0)
		}
	}

	//populate mines
	let populated = 0
	while (populated < mines) {
		let x = Math.floor(Math.random() * width)
		let y = Math.floor(Math.random() * length)

		if (field[y][x] == 0) {
			field[y][x] = 1
			populated++
		}
	}

	return field
}

function checkAdj(field, y, x) {
	adj = 0

	if (y-1 >= 0) {
		//check row of 3 above
		for (let i = x-1; i <= x+1; i++) {
			if (i >= 0 && i < field[y-1].length) {
				if (field[y-1][i] == 1) {
					adj++
				}
			}
		}
	}

	if (y+1 < field.length) {
		//check row of 3 below
		for (let i = x-1; i <= x+1; i++) {
			if (i >= 0 && i < field[y+1].length) {
				if (field[y+1][i] == 1) {
					adj++
				}
			}
		}
	}

	//check left & right
	if (x-1 >= 0) {
		if (field[y][x-1] == 1) {
			adj++
		}
	}
	if (x+1 < field[y].length) {
		if (field[y][x+1] == 1) {
			adj++
		}
	}

	return adj
}

function draw(field) {
	//X: hidden, 0-8: adjacent mines
	for (let i = 0; i < field.length; i++) {
		for (let j = 0; j < field[i].length; j++) {
			if (field[i][j] != 2) {
				process.stdout.write('X')
			} else {
					process.stdout.write(checkAdj(field, i, j).toString())
			}
		}
		process.stdout.write('\n')
	}
}

function drawRaw(field) {
	//*: mine, 0-8: adjacent mines
	for (let i = 0; i < field.length; i++) {
		for (let j = 0; j < field[i].length; j++) {
			if (field[i][j] == 1) {
				process.stdout.write('*')
			} else {
					process.stdout.write(checkAdj(field, i, j).toString())
			}
		}
		process.stdout.write('\n')
	}
}


function main() {
	let length = 5, width = 15, mines = 25 //keeping hardcoded values for now
	let safe = (length * width) - mines
	let cleared = 0

	let f = init(length, width, mines)
	draw(f)

	process.stdin.resume()
	process.stdin.setEncoding('utf8')

	process.stdout.write("Enter row and column (row col): ")

	process.stdin.on('data', (text) => {
		let index = text.trim().split(' ')
		let row = index[0]
		let col = index[1]

		if (f[row][col] == 1) {
			drawRaw(f)
			console.log("You Lose!")
			process.exit()
		} else {
			f[row][col] = 2
			cleared++

			if (cleared < safe) {
				draw(f)
				process.stdout.write("Enter row and column (row col): ")
			} else {
				drawRaw(f)
				console.log("You Win!")
				process.exit()
			}
		}
	})
}

main()
